package pl.wieleba;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User: piotr.wieleba
 */
public class XmlToHtmlTransformer {

    private final List<File> files;
    private TransformerFactory factory;

    public XmlToHtmlTransformer(List<File> files) {
        this.files = files;
        factory = TransformerFactory.newInstance();
        factory.setURIResolver(new ClassPathURIResolver());
    }

    public Result transform() {
        final Result xmlOutput = new StreamResult(new File("index.html"));
        try {
            Transformer transformer = factory.newTransformer(xsl("/citySearchResponse.xsl"));
            transformer.setParameter("INPUT_FILES", absolutePaths());
            transformer.transform(input(), xmlOutput);
            return xmlOutput;
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    private NodeList absolutePaths() {
        return makeXsltArg(files.stream().map(File::getAbsolutePath).collect(Collectors.toList()));
    }

    private Source input() {
        return files.stream().map(StreamSource::new).findFirst().get();
    }

    private Source xsl(String name) {
        return new StreamSource(XmlToHtmlTransformer.class.getResourceAsStream(name));
    }

    public static <T> NodeList makeXsltArg(Iterable<T> inputArgs) {
        DocumentBuilderFactory docBuildFact = DocumentBuilderFactory.newInstance();
        docBuildFact.setNamespaceAware(true);
        Document doc = getBuilder(docBuildFact).newDocument();
        doc.appendChild(doc.createElementNS(null, "root"));
        for (T item : inputArgs) {
            Element itemEl = doc.createElementNS(null, "item");
            itemEl.setTextContent(String.valueOf(item));
            doc.getDocumentElement().appendChild(itemEl);
        }
        return doc.getElementsByTagNameNS(null, "item");
    }

    private static DocumentBuilder getBuilder(DocumentBuilderFactory docBuildFact) {
        try {
            return docBuildFact.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

}
