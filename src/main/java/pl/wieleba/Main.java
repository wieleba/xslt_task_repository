package pl.wieleba;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User: piotr.wieleba
 */
public class Main {

    public static void main(String[] args) {
        try {
            new XmlToHtmlTransformer(existingXmlFiles(args)).transform();
        } catch (RuntimeException e) {
            System.out.printf("Error:\n%s", e.getMessage());
        }
    }

    private static List<File> existingXmlFiles(String[] args) {
        return Arrays.stream(args).map(Paths::get)
                .map(Path::toFile)
                .filter(File::exists)
                .collect(Collectors.toList());
    }
}
