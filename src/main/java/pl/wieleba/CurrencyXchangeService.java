package pl.wieleba;

import java.math.BigDecimal;

/**
 * User: piotr.wieleba
 */
public class CurrencyXchangeService {

    public static String xchangeToGBP(String value, String currencyCode) {
        final BigDecimal bigDecimalValue = new BigDecimal(value.trim());
        switch (currencyCode.trim()) {
            case "EUR":
                return String.valueOf(BigDecimal.valueOf(.77618D).multiply(bigDecimalValue));
            case "USD":
                return String.valueOf(BigDecimal.valueOf(.69389D).multiply(bigDecimalValue));
        }
        return String.valueOf(BigDecimal.ONE);
    }

}
