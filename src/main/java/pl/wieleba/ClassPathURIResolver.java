package pl.wieleba;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * User: piotr.wieleba
 */
class ClassPathURIResolver implements URIResolver {

    @Override
    public Source resolve(String href, String base) throws TransformerException {
        InputStream in = readResource(href);
        return new StreamSource(resourceNotFound(in) ? readFileStream(href) : in);
    }

    private boolean resourceNotFound(InputStream in) {
        return null == in;
    }

    private InputStream readFileStream(String name) {
        try {
            return new FileInputStream(new File(name));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private InputStream readResource(String name) {
        return ClassPathURIResolver.class.getResourceAsStream("/" + name);
    }
}
