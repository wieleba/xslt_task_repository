<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="head">
        <head>
            <title>Hotels</title>
            <style type="text/css">
                .containerBox {
                border: 1px solid black;
                }

                .medium_box {
                    width:550px;
                }

                #mainContainer li {
                list-style-position:inside;
                padding: 12px 45px;
                margin-bottom: 6px;
                border: 1px solid black;
                }

                .image {
                width: 100px;
                height: 100px;
                }

                .float_left {
                float: left;
                }

                .red {
                color: red;
                }

                .green {
                color: green;
                }

                .bold {
                font-weight: bold;
                }

                .grey_bcg {
                background: #575950;
                color: white;
                border: 0 solid black;
                border-collapse: collapse;
                }

                .row0 {
                background-color: #d6e2bb;
                }

                .row1 {
                background-color: #c8d3af;
                }
            </style>
        </head>
    </xsl:template>

</xsl:stylesheet>