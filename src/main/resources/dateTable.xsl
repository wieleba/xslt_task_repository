<xsl:stylesheet version="1.0"
                xmlns:xchange="http://xml.apache.org/xalan/java/pl.wieleba.CurrencyXchangeService"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="formatDate.xsl"/>

    <xsl:template name="dateTable" match="prices/price/dates/date">
        <xsl:call-template name="serviceDate"/>
        <xsl:call-template name="deadLine"/>
        <xsl:call-template name="charge"/>
        <xsl:call-template name="breakfastDescription"/>
        <xsl:call-template name="dailyPrice"/>
    </xsl:template>

    <xsl:template name="dailyPrice">
        <td>
            <table>
                <xsl:for-each select=".//dailyPrice">
                    <tr>
                        <td>
                            <xsl:value-of select="format-number(xchange:xchangeToGBP(.,./@currencyCode),'#.##')"/>GBP
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </td>
    </xsl:template>
    <xsl:template name="breakfastDescription">
        <td>
            <table>
                <xsl:for-each select=".//mealBasis/breakfast/breakfastDescription">
                    <tr>
                        <td>
                            <xsl:value-of select="."/>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </td>
    </xsl:template>
    <xsl:template name="charge">
        <td>
            <table>
                <xsl:for-each select=".//cancellationPolicy/charge">
                    <tr>
                        <td>
                            <xsl:value-of select="format-number(number(current()),'#')"/>%
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </td>
    </xsl:template>
    <xsl:template name="deadLine">
        <td>
            <table>
                <xsl:for-each select=".//cancellationPolicy/deadline">
                    <tr>
                        <td>
                            <xsl:call-template name="formatDate">
                                <xsl:with-param name="dateTime" select="."/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </td>
    </xsl:template>

    <xsl:template name="serviceDate">
        <td>
            <table>
                <xsl:for-each select=".//serviceDate">
                    <tr>
                        <td>
                            <xsl:call-template name="formatDate">
                                <xsl:with-param name="dateTime" select="."/>
                            </xsl:call-template>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </td>
    </xsl:template>

</xsl:stylesheet>
