<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


    <xsl:include href="repeat.xsl"/>

    <xsl:template name="hotelInformation" match="/">
        <div class="hotel_information">

            <xsl:apply-templates select="productInfo/hotelInfo/productDescription/productImage" mode="image_only"/>

            <div class="float_left">
                <xsl:apply-templates select="productInfo/hotelInfo"/>
            </div>
        </div>
        <div style="clear:both"/>
    </xsl:template>

    <xsl:template match="productImage" mode="image_only">

        <xsl:variable name="imgSrc">
            <xsl:value-of select="."/>
        </xsl:variable>

        <object class="image float_left" data="{$imgSrc}" type="image/jpg">
            <img class="image float_left"
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png"/>
        </object>
    </xsl:template>
    
    
    <xsl:template match="productDescription/productName">
        <h2>
            <xsl:value-of select="."/>
        </h2>
    </xsl:template>
    
    
    <xsl:template match="starRating">
        <xsl:variable name="rating">
            <xsl:value-of select="."/>
        </xsl:variable>

        <xsl:variable name="ratingSpan">
            <span class="red">☆</span>
        </xsl:variable>

        <span class="rating red">
            <xsl:call-template name="repeat">
                <xsl:with-param name="output" select="$ratingSpan"/>
                <xsl:with-param name="count" select="$rating"/>
            </xsl:call-template>
        </span>
    </xsl:template>
    
    
    <xsl:template match="productDescription/cityName">
        <div>
            <xsl:value-of select="."/>
        </div>
    </xsl:template>
    
    
    <xsl:template match="productDescription/productSummaryText">
        <div class="medium_box">
            <xsl:value-of select="."/>
        </div>
    </xsl:template>

    <!-- hide tangling attributes and text (hide broken url to image) -->
    <xsl:template match="text()|@*"/>

</xsl:stylesheet>
