<xsl:stylesheet version="1.0"
                xmlns:xdt="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="formatDate">
        <xsl:param name="dateTime" />
        <xsl:value-of select="xdt:format-date($dateTime,'dd MMM YYYY')" />
    </xsl:template>

</xsl:stylesheet>