<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="head.xsl"/>
    <xsl:include href="roomInformation.xsl"/>
    <xsl:include href="hotelInformation.xsl"/>

    <xsl:param name="INPUT_FILES"/>

    <xsl:template match="/">
        <html>
            <xsl:call-template name="head"/>
            <xsl:call-template name="body"/>
        </html>
    </xsl:template>

    <xsl:template name="body">
        <body>
            <ol id="mainContainer">
                <xsl:for-each select="document($INPUT_FILES)">
                    <xsl:call-template name="product_item"/>
                </xsl:for-each>
            </ol>
        </body>
    </xsl:template>

    <xsl:template name="product_item">
        <xsl:for-each select="//products/product">
            <li>
                <xsl:call-template name="hotelInformation"/>
                <xsl:call-template name="roomInformation"/>
            </li>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>