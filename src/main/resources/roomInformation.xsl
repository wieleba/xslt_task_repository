<xsl:stylesheet version="1.0"
                xmlns:xchange="http://xml.apache.org/xalan/java/pl.wieleba.CurrencyXchangeService"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:include href="dateTable.xsl"/>

    <xsl:template name="roomInformation" match="/">
        <div class="room_information">

            <table class="room_table" cellspacing="0">
                <thead class="grey_bcg">
                    <tr>
                        <th>Room Type</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Cxlc/l</th>
                        <th>Cxl chg</th>
                        <th>Meals</th>
                        <th>Price</th>
                        <th>Total Price</th>
                    </tr>
                </thead>
                <tbody>

                    <xsl:for-each select="subProducts/subProduct">
                        <xsl:variable name="pos" select="position() mod 2"/>

                        <tr class="row{$pos}">
                            <td>
                                <xsl:variable name="code" select="subProductCode"/>
                                <xsl:choose>
                                    <xsl:when test="$code = 'Twin Standard'">Twin Room</xsl:when>
                                    <xsl:when test="$code = 'Double Standard'">Double Room</xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$code"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <xsl:variable name="status" select="availabilityStatus"/>
                            <xsl:choose>
                                <xsl:when test="$status=1">
                                    <td class="green bold">Available</td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td class="red bold">Unavailable</td>
                                </xsl:otherwise>
                            </xsl:choose>

                            <xsl:call-template name="dateTable"/>

                            <td>
                                <xsl:variable name="totalPrice" select="prices/price/totalPrice"/>
                                <xsl:variable name="currencyCode" select="prices/price/totalPrice/@currencyCode"/>
                                <xsl:value-of select="format-number(xchange:xchangeToGBP($totalPrice,$currencyCode),'#.##')"/>GBP
                            </td>

                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </div>
    </xsl:template>

</xsl:stylesheet>
